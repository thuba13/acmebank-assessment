import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {AppMaterialModule} from "./app-material.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Store} from '@ngrx/store';
import {BehaviorSubject} from "rxjs/index";

import {AccountsComponent} from "./accounts/accounts/accounts.component";
import {AccountListComponent} from "./accounts/account-list/account-list.component";
import {TotalBalanceComponent} from "./accounts/total-balance/total-balance.component";
import {Account} from './models/account';

describe('AppComponent', () => {
  let storeSpy: any;
  let totalBalanceSubject: BehaviorSubject<number>;
  let accountSubject: BehaviorSubject<Account>;

  beforeEach(async(() => {
    storeSpy = jasmine.createSpyObj('store', ['dispatch', 'select']);

    TestBed.configureTestingModule({
      imports: [AppMaterialModule, BrowserAnimationsModule],
      declarations: [
        AppComponent, AccountsComponent, AccountListComponent, TotalBalanceComponent
      ],
      providers: [
        {
          provide: Store,
          useValue: storeSpy
        }
      ],
    }).compileComponents();

    totalBalanceSubject = new BehaviorSubject<number>(200);
    accountSubject = new BehaviorSubject<Account>(createMockAccount());

    storeSpy.select.and.callFake(() => totalBalanceSubject.asObservable());
    storeSpy.select.and.callFake(() => accountSubject.asObservable());
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});

function createMockAccount() {
  const mock: Account = {
    account_number: '6331103626640816',
    account_type: 'cheque',
    balance: '-296.65'
  };

  return mock;
}
