import { Account } from './account';

export class AccountModel {

  constructor(private account: Account) {
  }

  get canWithdraw(): boolean {
    return  (+this.account.balance > 0 && this.account.account_type === 'savings') ||
            (+this.account.balance > -500 && this.account.account_type === 'cheque');
  }
}
