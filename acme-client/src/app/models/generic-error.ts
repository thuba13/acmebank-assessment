export interface GenericError {
  errorCode: number;
  errorDescription: string;
  internalError: any;
}
