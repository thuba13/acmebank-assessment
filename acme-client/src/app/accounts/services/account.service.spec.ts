import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from "../../../environments/environment";

import {AccountService} from "./account.service";

describe('AccountService', () => {
  let httpMock: HttpTestingController;
  let accountService: AccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AccountService]
    });

    httpMock = TestBed.get(HttpTestingController);
    accountService = TestBed.get(AccountService);
  });

  it('should fetch accounts on getAccountList call', (done) => {
    accountService.getAccountList()
      .subscribe((res: any) => {
        expect(res).toEqual([]);
        done();
      });

    const request = httpMock.expectOne(`${environment.apiEndpoint}`);
    request.flush([]);

    httpMock.verify();
  });
});
