import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

import {Account} from '../../models/account';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  accountList: Observable<Account[]>;

  constructor(private httpClient : HttpClient) { }

  getAccountList() : Observable<Account[]>{
    this.accountList = this.httpClient.get<Account[]>(`${environment.apiEndpoint}`);
    return this.accountList;
  }
}
