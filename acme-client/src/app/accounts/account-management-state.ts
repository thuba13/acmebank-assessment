import {AccountState} from "./store/account.reducer";

export interface AccountManagementState {
  accounts: AccountState;
}
