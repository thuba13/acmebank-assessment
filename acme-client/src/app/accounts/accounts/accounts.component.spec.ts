import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {BehaviorSubject} from "rxjs/index";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppMaterialModule} from "../../app-material.module";

import {AccountsComponent} from "./accounts.component";
import {AccountListComponent} from "../account-list/account-list.component";
import {TotalBalanceComponent} from "../total-balance/total-balance.component";
import {Account} from '../../models/account';


describe('AccountsComponent', () => {
  let component: AccountsComponent;
  let fixture: ComponentFixture<AccountsComponent>;
  let storeSpy: any;
  let totalBalanceSubject: BehaviorSubject<number>;
  let accountsSubject: BehaviorSubject<Account[]>;

  beforeEach(async(() => {
    storeSpy = jasmine.createSpyObj('store', ['dispatch', 'select']);

    TestBed.configureTestingModule({
        imports: [AppMaterialModule, BrowserAnimationsModule],
        declarations: [AccountsComponent, AccountListComponent, TotalBalanceComponent],
        providers: [
          {
            provide: Store,
            useValue: storeSpy
          }
        ],
      })
      .compileComponents();

    totalBalanceSubject = new BehaviorSubject<number>(100);
    accountsSubject = new BehaviorSubject<Account[]>(createMockAccounts());

    storeSpy.select.and.callFake(() => totalBalanceSubject.asObservable());
    storeSpy.select.and.callFake(() => accountsSubject.asObservable());
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    accountsSubject.complete();
    totalBalanceSubject.complete();
  });

  it('should create the AccountsComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should contain the account list and total balance components', () => {
    expect(fixture.debugElement.nativeElement.querySelector('app-account-list')).not.toBe(null);
    expect(fixture.debugElement.nativeElement.querySelector('app-total-balance')).not.toBe(null);
  });
});

function createMockAccounts() {
  const mocks: Account[] = [
    {
      account_number: '6331103626640816',
      account_type: 'cheque',
      balance: '-296.65'
    },
    {
      account_number: '5248117462997084',
      account_type: 'savings',
      balance: '-20.00'
    }];

  return mocks;
}
