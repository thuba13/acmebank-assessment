import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {BehaviorSubject} from "rxjs/index";
import {By} from '@angular/platform-browser';
import {AppMaterialModule} from "../../app-material.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AccountListComponent} from "./account-list.component";
import {FetchAllAccounts} from "../store/account.actions";
import {Account} from '../../models/account';

describe('AccountListComponent', () => {
  let component: AccountListComponent;
  let fixture: ComponentFixture<AccountListComponent>;
  let accountsSubject: BehaviorSubject<Account[]>;
  let storeSpy: any;
  let expectedAccounts: Account[];

  beforeEach(async(() => {
    storeSpy = jasmine.createSpyObj('store', ['dispatch', 'select']);

    TestBed.configureTestingModule({
        declarations: [ AccountListComponent ],
        imports: [AppMaterialModule, BrowserAnimationsModule],
        providers: [
          {
            provide: Store,
            useValue: storeSpy
          }
        ],
      })
      .compileComponents();

    expectedAccounts = createMockAccounts();
    accountsSubject = new BehaviorSubject<Account[]>(expectedAccounts);
    storeSpy.select.and.callFake(() => accountsSubject.asObservable());
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    accountsSubject.complete();
  });

  it('should create the AccountListComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should request all accounts from the store', () => {
    expect(storeSpy.dispatch).toHaveBeenCalledWith(new FetchAllAccounts());
  });

  it('should render the account data', () => {
    const el = fixture.debugElement.queryAll(By.css('td'));
    expect(el[0].nativeElement.textContent).toEqual(expectedAccounts[0].account_number);
    expect(el[1].nativeElement.textContent).toEqual(expectedAccounts[0].account_type);
    expect(el[2].nativeElement.textContent).toEqual(expectedAccounts[0].balance);
  });

  it('should disable the button of a negative savings account', () => {
    const buttons = fixture.debugElement.queryAll(By.css('button'));
    expect(buttons[1].nativeElement.disabled).toEqual(true);
  });

  describe('And the Withdraw button is clicked', () => {
    it('Then the onWithdraw method should be called', () => {
      spyOn(component, 'onWithdraw');

      const firstRowWithdrawButton = fixture.debugElement.queryAll(By.css('button'))[0];
      firstRowWithdrawButton.triggerEventHandler('click', null);

      expect(component.onWithdraw).toHaveBeenCalled();
    });
  });
});


function createMockAccounts() {
  const mocks: Account[] = [
    {
      account_number: '6331103626640816',
      account_type: 'cheque',
      balance: '-296.65'
    },
    {
      account_number: '5248117462997084',
      account_type: 'savings',
      balance: '-20.00'
    },
    {
      account_number: '3581474249964105',
      account_type: 'savings',
      balance: '980.20'
    }];

  return mocks;
}
