import { Component, OnInit, OnDestroy } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable, Subscription} from "rxjs/index";
import {MatSnackBar} from '@angular/material';

import {Account} from '../../models/account';
import {FetchAllAccounts, SelectAccount} from "../store/account.actions";
import {AccountManagementState} from "../account-management-state";
import {selectAllAccounts} from "../account-management-store";
import {AccountModel} from "../../models/account-model";

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit, OnDestroy {
  private _accountsSub: Subscription;
  private accountModels: AccountModel[];

  constructor(
    private _store : Store<AccountManagementState>,
    private _snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    this._store.dispatch(new FetchAllAccounts());
    this._accountsSub = this._store.select(selectAllAccounts).subscribe(accounts => {
      this.accountModels = [];
      accounts.forEach(account => {
        this.accountModels.push(new AccountModel(account));
      });
    });
  }

  ngOnDestroy() {
    this._accountsSub.unsubscribe();
  }

  onWithdraw(account: Account)
  {
    this._store.dispatch(new SelectAccount(account));

    const displayDuration = 3000;
    let snackBarRef = this._snackBar.open('Success', '', {
      duration: displayDuration
    });
  }
}

