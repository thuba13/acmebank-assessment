import {Component, OnInit, OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import {Subscription} from "rxjs/index";

import {AccountManagementState} from "../account-management-state";
import {selectTotalBalance} from "../account-management-store";

@Component({
  selector: 'app-total-balance',
  templateUrl: './total-balance.component.html',
  styleUrls: ['./total-balance.component.css']
})
export class TotalBalanceComponent implements OnInit, OnDestroy {
  private _totalBalanceSub: Subscription;
  private totalBalance: number;

  constructor(private _store: Store<AccountManagementState>) { }

  ngOnInit() {
    this._totalBalanceSub = this._store.select(selectTotalBalance).subscribe(bal => this.totalBalance = bal);
  }

  ngOnDestroy() {
    this._totalBalanceSub.unsubscribe();
  }
}
