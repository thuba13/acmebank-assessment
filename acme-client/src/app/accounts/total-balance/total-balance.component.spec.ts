import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Store} from '@ngrx/store';
import {BehaviorSubject} from "rxjs/index";
import {By} from '@angular/platform-browser';

import {TotalBalanceComponent} from "./total-balance.component";

describe('TotalBalanceComponent', () => {
  let component: TotalBalanceComponent;
  let fixture: ComponentFixture<TotalBalanceComponent>;
  let totalBalanceSubject: BehaviorSubject<number>;
  let storeSpy: any;
  let expectedBalance = 100;

  beforeEach(async(() => {
    storeSpy = jasmine.createSpyObj('store', ['dispatch', 'select']);
    TestBed.configureTestingModule({
        declarations: [ TotalBalanceComponent ],
        imports: [],
        providers: [
          {
            provide: Store,
            useValue: storeSpy
          }
        ],
      })
      .compileComponents();

    totalBalanceSubject = new BehaviorSubject<number>(expectedBalance);
    storeSpy.select.and.callFake(() => totalBalanceSubject.asObservable());
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    totalBalanceSubject.complete();
  });

  it('should create the TotalBalanceComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should render the Balance heading', () => {
    const el = fixture.debugElement.queryAll(By.css('.h4'));
    expect(el[0].nativeElement.textContent).toEqual('Balance');
  });

  it('should render the correct balance', () => {
    const el = fixture.debugElement.queryAll(By.css('td'));
    expect(el[2].nativeElement.textContent).toEqual(expectedBalance.toString());
  });

});
