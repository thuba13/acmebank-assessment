import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import {reducers, metaReducers} from "./account-management-store";
import {AccountEffects} from "./store/account.effects";



@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('accounts', reducers, {metaReducers}),
    EffectsModule.forFeature([AccountEffects]),
  ],
  declarations: []
})
export class AccountsModule { }
