import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import {AccountState, accountReducer, accountAdapter} from "./store/account.reducer";
import {AccountManagementState} from "./account-management-state";

export const reducers: ActionReducerMap<AccountManagementState> = {
  accounts : accountReducer
};

export function logger (reducer: ActionReducer<AccountManagementState>): ActionReducer<AccountManagementState> {
  return function (state: AccountManagementState, action: any): AccountManagementState {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<AccountManagementState>[] = !environment.production
  ? [logger]
  : [];

const selectAccountManagementState = createFeatureSelector<AccountManagementState>('accounts');

const selectAccountsState =  createSelector(
  selectAccountManagementState,
  (state: AccountManagementState) => state.accounts
);

export const {
  selectAll: selectAllAccounts
} = accountAdapter.getSelectors(selectAccountsState);

export const selectSelectedAccount = createSelector(
  selectAccountsState,
  (state: AccountState) => state.SelectedAccount
);

export const selectTotalBalance = createSelector(
  selectAccountsState,
  (state: AccountState) => state.TotalBalance
);
