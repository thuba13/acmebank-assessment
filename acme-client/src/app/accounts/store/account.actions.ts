import { Action } from '@ngrx/store';
import { Account } from '../../models/account';
import {GenericError} from "../../models/generic-error";

export enum AccountActionTypes {
  FetchAllAccounts = '[Account] Fetch All Accounts',
  FetchAllAccountsSuccess = '[Account] Fetch All Accounts Success',
  FetchAllAccountsError = '[Account] Fetch All Accounts Error',
  SelectAccount = '[Account] Select Account'
}

export class FetchAllAccounts implements Action {
  readonly type = AccountActionTypes.FetchAllAccounts;

  constructor() {}
}

export class FetchAllAccountsSuccess implements Action {
  readonly type = AccountActionTypes.FetchAllAccountsSuccess;

  constructor(public payload: Account[]) {}
}

export class FetchAllAccountsError implements Action {
  readonly type = AccountActionTypes.FetchAllAccountsError;

  constructor(public payload: GenericError) {}
}

export class SelectAccount implements Action {
  readonly type = AccountActionTypes.SelectAccount;

  constructor(public payload: Account) {}
}

export type AccountActions =
      FetchAllAccounts
    | FetchAllAccountsSuccess
    | FetchAllAccountsError
    | SelectAccount
