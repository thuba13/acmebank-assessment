import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from "rxjs";
import {catchError, map, mergeMap} from 'rxjs/operators';
import {Action} from "@ngrx/store";

import {
  AccountActionTypes, FetchAllAccounts, FetchAllAccountsSuccess, FetchAllAccountsError
} from "./account.actions";
import {AccountService} from "../services/account.service";
import {GenericError} from "../../models/generic-error";

@Injectable()
export class AccountEffects {

  constructor(private _actions$: Actions,
              private _accountService: AccountService) {}

  @Effect()
  fetchAccounts$: Observable<Action> = this._actions$.pipe(
    ofType<FetchAllAccounts>(AccountActionTypes.FetchAllAccounts),
    mergeMap(action =>
      this._accountService.getAccountList().pipe(
        map(response => new FetchAllAccountsSuccess(response)),
        catchError((err: HttpErrorResponse) => {
          return of(new FetchAllAccountsError(<GenericError>{
            errorCode: err.status,
            errorDescription: err.statusText,
            internalError: err
          }));
        })
      )
    )
  );
}
