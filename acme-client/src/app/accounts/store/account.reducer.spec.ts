import {FetchAllAccountsSuccess, SelectAccount} from "./account.actions";
import {initialState, accountReducer} from "./account.reducer";
import {Account} from '../../models/account';

describe('accountReducer', () => {
  it('should update the state for FetchAllAccountsSuccess action', () => {
    const testData = createMockAccounts();
    const actionToPerform = new FetchAllAccountsSuccess(testData);
    const stateBefore = initialState;

    const stateAfter = accountReducer(stateBefore, actionToPerform);

    expect(stateAfter.ids).toEqual(testData.map(item => item.account_number));
    expect(stateAfter.TotalBalance.toFixed(2)).toBe('663.55');
  });
});


describe('accountReducer', () => {
  it('should update the state for SelectAccount action', () => {
    const testData = createMockAccounts()[0];
    const actionToPerform = new SelectAccount(testData);
    const stateBefore = initialState;

    const stateAfter = accountReducer(stateBefore, actionToPerform);

    expect(stateAfter.SelectedAccount.account_number).toBe('6331103626640816');
    expect(stateAfter.SelectedAccount.account_type).toBe('cheque');
    expect(stateAfter.SelectedAccount.balance).toBe('-296.65');
  });
});


function createMockAccounts() {
  return <Account[]>[
    {
      account_number: '6331103626640816',
      account_type: 'cheque',
      balance: '-296.65'
    },
    {
      account_number: '5248117462997084',
      account_type: 'savings',
      balance: '-20.00'
    },
    {
      account_number: '3581474249964105',
      account_type: 'savings',
      balance: '980.20'
    }];
}
