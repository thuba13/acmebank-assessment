import {EntityState, EntityAdapter, createEntityAdapter} from '@ngrx/entity';
import {Account} from "../../models/account";
import {AccountActionTypes,AccountActions} from "./account.actions";

export interface AccountState extends EntityState<Account> {
  SelectedAccount: Account;
  TotalBalance: number;
}

export const accountAdapter: EntityAdapter<Account> = createEntityAdapter<Account>({
  selectId: (account: Account) => account.account_number,
  sortComparer: false
});

export const initialState: AccountState = accountAdapter.getInitialState({
  SelectedAccount: null,
  TotalBalance: null
});

export function accountReducer(
  state = initialState,
  action: AccountActions
): AccountState {
  switch (action.type) {
    case AccountActionTypes.FetchAllAccountsSuccess:
      return {
        ...accountAdapter.addMany(action.payload, state),
        TotalBalance: action.payload.reduce((total : number, account: Account) => {
          return (total) + (+account.balance);
        },0)
      };

    case AccountActionTypes.SelectAccount:
      return {
        ...state,
        SelectedAccount: action.payload
      };

    //TODO: OnLive: I would Implement more cases for exeption handling e.g. AccountActionTypes.FetchAllAccountsError (to handle api errors gracefully and display message to client)

    default:
      return state;
  }
}
