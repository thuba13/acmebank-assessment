import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from './app.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import {AppMaterialModule} from "./app-material.module";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AccountService } from './accounts/services/account.service';
import { AccountsModule } from './accounts/accounts.module';
import { AccountsComponent } from './accounts/accounts/accounts.component';
import { AccountListComponent } from './accounts/account-list/account-list.component';
import { TotalBalanceComponent } from './accounts/total-balance/total-balance.component';



@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    AccountListComponent,
    TotalBalanceComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    AccountsModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
