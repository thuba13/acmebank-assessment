#Acme Bank

The back end uses the provided mock api server
The front end is written in angular 6

## Getting started	

API:
Navigate to the mock-api-server folder, install dependencies and run as follows:
npm install

npm run dev


CLIENT:
Navigate to the acme-client folder, install dependencies and run as follows:

npm install

ng serve --open

